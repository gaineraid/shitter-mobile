import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native'
import { Container, Header, Content, Text as AltText, Footer, FooterTab, Button, Icon } from 'native-base'
import * as Expo from 'expo'

var ic = require('./poo.png')

export default class App extends React.Component {
  constructor() {
      super();
      this.state = {
            isReady: false
          };
    }
    componentWillMount() {
        this.loadFonts();
      }
    async loadFonts() {
        await Expo.Font.loadAsync({
              Roboto: require("native-base/Fonts/Roboto.ttf"),
              Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
              Ionicons: require("@expo/vector-icons/fonts/Ionicons.ttf")
            });
        this.setState({ isReady: true });
      }
  render() {
    if(this.state.isReady){
      return <Expo.AppLoading />
    }
    return (
      <Container>
        <Header />
        <Content>
          <View style={styles.container}>
            <Text style={{fontSize: 30}}>
              Test!
            </Text>
          </View>
        </Content>
        <Footer>
          <FooterTab>
            <Button primary>
              <AltText>My Shits</AltText>
              <Image source={ic} />
            </Button>
            <Button>
              <Icon name="camera" />
            </Button>
            <Button active>
              <Icon active name="navigate" />
            </Button>
            <Button>
              <Icon name="person" />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
