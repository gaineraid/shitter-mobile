import axios from 'axios'

//const url = 'http://localhost:3000'
const url = 'http://104.236.168.226:3022'

class ShitterApi {
  get = (controller) => {
    return axios({
      method: 'get',
      headers:{
        'X-Auth-Token': `eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwiZW1haWwiOiJnYWluZXJhaWRAZ21haWwuY29tIiwicGhvbmUiOiI2MDEyNDk3NTM0In0.g-4-RPIQbhcbp5h6-NZO_m4G0mTe_gbyqZJVcEYPKfQ`,
      },
      url: `${url}/api/${controller}`,
    })
  }
  post = (controller, data) => {
    return axios({
      method: 'post',
      data: data,
      headers:{
        'X-Auth-Token': `eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwiZW1haWwiOiJnYWluZXJhaWRAZ21haWwuY29tIiwicGhvbmUiOiI2MDEyNDk3NTM0In0.g-4-RPIQbhcbp5h6-NZO_m4G0mTe_gbyqZJVcEYPKfQ`,
      },
      url: `${url}/api/${controller}`,
    })
  }
}

export default new ShitterApi()
