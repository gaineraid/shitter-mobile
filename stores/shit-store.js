import { observable } from 'mobx'
import { computed }   from 'mobx'
import { action }     from 'mobx'
import ShitterApi     from '../api/shitter-api'

class ShitStore {
  @observable shits = []
  @observable error = null

  @action.bound
  getShits(){
    ShitterApi.get('shits')
      .then(
        action( res => { this.shits = res.data } ),
        action( err => { this.error = err } )
      )
  }
}

export default new ShitStore()
