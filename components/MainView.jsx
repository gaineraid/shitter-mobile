import React          from 'react'
import { StyleSheet } from 'react-native'
import { Text }       from 'react-native'
import { View }       from 'react-native'
import { Image }      from 'react-native'
import { FlatList }   from 'react-native'
import { List }       from 'react-native-elements'
import { ListItem }   from 'react-native-elements'
import { Container, Header, Content, Text as AltText, Footer, FooterTab, Button, Icon } from 'native-base'
import ShitterApi from '../api/shitter-api'
const R = require('ramda')

//@inject("ShitStore")
//@observer
class MainView extends React.Component{
  constructor(){
    super()
    this.state = { lat: "", lon: "", alt: "", shits: [] }
  }

  componentDidMount(){
    this.getShit()
  }
  getShit = () => {
    ShitterApi.get('shits')
      .then(
        res => {
          this.setState({shits: res.data})
        },
        err => {
          this.setState({err: err})
        }
      )
  }

  getLocation = () => {
    navigator.geolocation.getCurrentPosition(
      (position) => {
        console.log(position.coords)
        this.setState({
          current_shit: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            altitude: position.coords.altitude,
          }
        })
        ShitterApi.post('shits', this.state.current_shit)
        this.getShit()
      },
      (err) => { null },
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    )
  }

  render(){
    return (
      <View>
        <Button onPress={this.getLocation} style={{alignSelf: 'center', marginTop: 8}}>
          <AltText>Take a Shit</AltText>
        </Button>
        <List>
          <FlatList
            data={this.state.shits}
            keyExtractor={item => item.id.toString()}
            renderItem={ ({item}) => (
              <ListItem
                roundAvatar
                avatar={ item.user_id === 1 ? require('../me_avitar.png') : require('../jeff_avitar.png')}
                hideChevron
                title={item.coordinates} 
              /> 
            )}
          />
        </List>
      </View>
    )
  }
}


export default MainView
/*
        <Text> Coordinates: { R.path([0, 'coordinates'], this.state.shits) }</Text>
        <Text>Latitude: {this.state.lat} </Text>
        <Text> Longitude: {this.state.lon} </Text>
        <Text> Altitude: {this.state.alt}</Text>
*/
