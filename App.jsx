import React from 'react'
import { StyleSheet } from 'react-native'
import { Text } from 'react-native'
import { View } from 'react-native'
import { Image } from 'react-native'
import MainView from './components/MainView'
import { Container, Header, Content, Text as AltText, Footer, FooterTab, Button, Icon } from 'native-base'
import ShitterApi from './api/shitter-api'

const R = require('ramda')
var ic = require('./poo.png')

class App extends React.Component {
  constructor(){
    super()
    this.state = { loading: true, lat: "", lon: "", alt: "", shits: [] }
  }
  async componentWillMount() {
    await Expo.Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    });
    this.setState({loading: false})
  }
  componentDidMount(){
  }
  render() {
    if(this.state.loading){ return <Text>Loading</Text> }
    return (
      <Container>
        <Header />

        <Content contentContainerStyle={styles.container}>
          <MainView />
        </Content>

        <Footer>
          <FooterTab>
            <Button>
              <AltText>My Shits</AltText>
              <Image source={ic} />
            </Button>
            <Button>
              <AltText>Buddy Shits</AltText>
              <Image source={ic} />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexGrow: 1,
    width: '100%',
    backgroundColor: '#fff',
    alignSelf: 'stretch',
    alignItems: 'stretch',
  },
});

export default App
